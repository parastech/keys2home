﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Key2Home.Common
{
    public class EmailManager
    {

        public static void AppSettings(out string UserID, out string Password, out string SMTPPort, out string Host)
        {
            UserID = ConfigurationManager.AppSettings.Get("smtpUserName");
            Password = ConfigurationManager.AppSettings.Get("smtpPassword");
            SMTPPort = ConfigurationManager.AppSettings.Get("SMTPPort");
            Host = ConfigurationManager.AppSettings.Get("smtpHost");
        }
        public static void SendEmail(string From, string Subject, string Body, string To, string UserID, string Password, string SMTPPort, string Host)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add(To);
            mail.Subject = Subject;
            mail.Body = Body;
            SmtpClient smtp = new SmtpClient();
            smtp.Port = Convert.ToInt16(SMTPPort);
            smtp.Host = Host;
            mail.From = new MailAddress(From);
            smtp.Credentials = new NetworkCredential(UserID, Password);
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
    public class EmailBodyForSendDriverApproveTemplate
    {
        public static string createEmailBody(string Template)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            string str = string.Format("{0}", Template);
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(str)))
            {
                body = reader.ReadToEnd();
            }
            return body;
        }
    }
    public class EmailBodyForSendForgotTemplate
    {
        public static string createEmailBody(Guid code, DateTime token, string email, string Template)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            string str = string.Format("{0}", Template);
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(str)))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{code}", code.ToString());
            body = body.Replace("{email}", email);
            body = body.Replace("{ResetToken}", token.ToString());
            return body;
        }
    }
    public class EmailBody
    {
        public static string createEmailBody(string type, string message, string Template)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   
            string str = string.Format("~/HtmlTemplate/{0}", Template);
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(str)))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{message}", message);
            body = body.Replace("{type}", type);
            return body;
        }
    }
}
