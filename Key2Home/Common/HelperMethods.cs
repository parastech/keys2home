﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Key2Home.Common
{
    public class HelperMethods
    {
        static string cipherText = ConfigurationManager.AppSettings["CipherText"];
        /// <summary>
        /// Encrypt Password
        /// </summary>
        /// <param name="clearText"></param>
        /// <returns></returns>
        public static string Encrypt(string clearText)
        {
            try
            {
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(cipherText, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Decrypt Password
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string Decrypt(string password)
        {
            try
            {
                byte[] cipherBytes = Convert.FromBase64String(password);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(cipherText, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        password = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return password;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to send mails
        /// </summary>
        /// <param name="userMailIds"></param>
        /// <param name="body"></param>
        /// <param name="name"></param>
        /// <param name="subject"></param>
        public static void SendMail(List<string> userMailIds, string body, string name, string subject)
        {
            try
            {
                var mailMessage = new MailMessage
                {
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = body,
                    From = new MailAddress("")
                };
                if (userMailIds != null)
                {
                    foreach (var emailId in userMailIds)
                    {
                        mailMessage.To.Add(new MailAddress(emailId));
                    }
                }
                var userName = ConfigurationManager.AppSettings["Username"];
                var userPassword = ConfigurationManager.AppSettings["Password"];
                var smtp = new SmtpClient { Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]), EnableSsl = true, Credentials = new NetworkCredential(userName, userPassword), Host = ConfigurationManager.AppSettings["Host"], DeliveryMethod = SmtpDeliveryMethod.Network };

                smtp.SendAsync(mailMessage, "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
