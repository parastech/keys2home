﻿function initDatatable() {
    $('#Users').DataTable({
        "destroy": true,
       // "processing": true,
        "serverSide": false,
        //"bStateSave": true,
        "info": true,
        "lengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ],
        //"order": [[0, "asc"]],
        "filter": true,
        "ajax": {
            "url": '/User/GetActiveCustomerList',
            "type": "GET",
            "datatype": "json",
            success: function (data) {
              
            },
            "error": function (xhr, error, thrown) {
               
            }
        },
        "columns": [
            {
                "data": "AdminName",
                "name": "0",
                "autoWidth": true,
                "orderable": false,
                "sorting": false

            },
            {
                "data": "AdminEmail",
                "name": "1",
                "autoWidth": true
            },
            {
                "data": "Phone",
                "name": "2",
                "autoWidth": true

            },
            {
                "data": "AdminID",
                "name": "3",
                "autoWidth": true,
                "render": function (data) {
                    return "<a class='btn btn-primary btn_hover_shadow btn-sm' id=agent_detail data-id= " + data + ">&nbsp;View Details</a>"
                }
            },
            {
                "data": "AdminID",
                "width": "50px",
                "orderable": false,
                "sorting": false,
                "render": function (data) {
                    return "<a style='margin: 0px; text - align: center' class='btn btn-orange' id=Deactivate= " + data + ">&nbsp;Deactivate</a>"
                }
            }]
    });
}


$(document).ready(function () {
  
    initDatatable();
});