﻿using Key2Home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Key2Home.Models;
using System.IO;
using System.Data.Entity;

namespace Key2Home.Controllers
{
    public class AdminController : Controller
    {

        KeyToHomeEntities db = new KeyToHomeEntities();

        // GET: Admin
        public ActionResult Index()
        {
            try
            {
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }

        [HttpGet]
        public ActionResult AddAdmin()
        {
            var list = new List<SelectListItem>
        {
        new SelectListItem{ Text="LoanOffcier"},
        new SelectListItem{ Text="SuperAdmin"},
        new SelectListItem{ Text="SalesOfficer" },
        new SelectListItem{ Text="MainAgent"}
        };
            ViewData["TypeList"] = list;
            return View();
        }

        [HttpPost]
        public ActionResult AddAdmin(admin addAdmin, HttpPostedFileBase fileS)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    admin obj = new admin();
                    string imagefileName = string.Empty;
                    if (fileS != null && fileS.ContentLength > 0)
                    {
                        imagefileName = Path.GetFileName(fileS.FileName);
                        string Extension = Path.GetExtension(imagefileName);

                        string ImageName = System.IO.Path.GetFileName(fileS.FileName);
                        string PhysicalPath = Server.MapPath("/Key2HomeImages/" + ImageName);
                        fileS.SaveAs(PhysicalPath);
                        obj.AdminImage = imagefileName;
                    }
                    obj.Type = addAdmin.Type;
                    obj.AdminName = addAdmin.AdminName;
                    obj.AdminAddress = addAdmin.AdminAddress;
                    obj.AdminEmail = addAdmin.AdminEmail;
                    obj.AdminPassword = addAdmin.AdminPassword;
                    obj.CreateOn = DateTime.Now;
                    obj.PostDesc = addAdmin.PostDesc;
                    obj.Phone = addAdmin.Phone;
                    obj.MLSID = addAdmin.MLSID;
                    obj.PLans = addAdmin.PLans;
                    obj.AdminStatusID = "1";
                    //obj.AdminStatusID = Convert.(true);
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.AppXID = 1;
                    db.admins.Add(obj);
                    db.SaveChanges();
                    ModelState.Clear();
                    return RedirectToAction("Index");
                }
                var list = new List<SelectListItem>
        {
        new SelectListItem{ Text="LoanOffcier"},
        new SelectListItem{ Text="SuperAdmin"},
        new SelectListItem{ Text="SalesOfficer" },
        new SelectListItem{ Text="MainAgent"}
        };
                ViewData["TypeList"] = list;
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Admin(int id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditAdmin(int id)
        {
            try
            {
                var AdminData = db.admins.Where(m => m.AdminID == id).FirstOrDefault();
                var list = new List<SelectListItem>
        {
        new SelectListItem{ Text="LoanOffcier"},
        new SelectListItem{ Text="SuperAdmin"},
        new SelectListItem{ Text="SalesOfficer" },
        new SelectListItem{ Text="MainAgent"}
        };
                ViewData["TypeList"] = list;
                return View(AdminData);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost]
        public ActionResult EditAdmin(admin EditAdmin, HttpPostedFileBase fileS)
        {
            try
            {
                var Admindata = db.admins.Where(m => m.AdminID == EditAdmin.AdminID).FirstOrDefault();
                if (ModelState.IsValid)
                {
                    if (fileS == null)
                    {
                        EditAdmin.AdminImage = Admindata.AdminImage;
                    }
                    string imagefileName = string.Empty;
                    if (fileS != null && fileS.ContentLength > 0)
                    {
                        imagefileName = Path.GetFileName(fileS.FileName);
                        string Extension = Path.GetExtension(imagefileName);
                        string ImageName = System.IO.Path.GetFileName(fileS.FileName);
                        string PhysicalPath = Server.MapPath("/Key2HomeImages/" + ImageName);
                        fileS.SaveAs(PhysicalPath);
                        Admindata.AdminImage = imagefileName;
                    }               
                    Admindata.Type = EditAdmin.Type;
                    Admindata.AdminName = EditAdmin.AdminName;
                    Admindata.AdminEmail = EditAdmin.AdminEmail;
                    Admindata.AdminAddress = EditAdmin.AdminAddress;
                    Admindata.AdminPassword = EditAdmin.AdminPassword;
                    Admindata.Phone = EditAdmin.Phone;
                    Admindata.PostDesc = EditAdmin.PostDesc;
                    Admindata.PLans = EditAdmin.PLans;
                    Admindata.MLSID = EditAdmin.MLSID;
                    Admindata.IsActive = EditAdmin.IsActive;
                    Admindata.AdminStatusID = EditAdmin.AdminStatusID;
                    Admindata.IsDeleted = EditAdmin.IsDeleted;
                    Admindata.AppXID = EditAdmin.AppXID;
                    Admindata.CreateOn = EditAdmin.CreateOn;
                    db.Entry(Admindata).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    var list = new List<SelectListItem>
        {
        new SelectListItem{ Text="LoanOffcier"},
        new SelectListItem{ Text="SuperAdmin"},
        new SelectListItem{ Text="SalesOfficer" },
        new SelectListItem{ Text="MainAgent"}
        };
                    ViewData["TypeList"] = list;
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult GetAllAdmins()
        {
            try
            {
                var getAllAdmin = db.admins.OrderByDescending(i => i.CreateOn).Where(i => i.IsActive == true).ToList();
                // var getAllAdmin = db.admins.OrderByDescending(i => i.CreateOn).ToList();
                if (getAllAdmin.Count() > 0)
                {
                    ViewBag.RecordExists = "true";
                }
                return Json(getAllAdmin, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult DeleteAdmin(int ? delAdminId)
        {
            try
            {
                if (delAdminId != null)
                {
                    var DelAdmin = db.admins.Single(s => s.AdminID == delAdminId);
                    DelAdmin.IsActive = false;
                    db.Entry(DelAdmin).State = EntityState.Modified;
                    db.SaveChanges();
              
                    var getRemAdmins = db.admins.OrderByDescending(i => i.CreateOn).Where(i => i.IsActive == true).ToList();
                   
                    return Json(getRemAdmins, JsonRequestBehavior.AllowGet);
                }
                // var getAllAdmins = db.admins.OrderByDescending(i => i.CreateOn).ToList();
                var getAllAdmins = db.admins.OrderByDescending(i => i.CreateOn).Where(i => i.IsActive == true).ToList();
                return Json(getAllAdmins, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }        
        }

    }
}