﻿using Key2Home.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Key2Home.Controllers
{
    public class CampaignController : Controller
    {
        KeyToHomeEntities db = new KeyToHomeEntities();
        // GET: Campaign
        public ActionResult Index()
        {
            try
            {           
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
    
        [HttpGet]
        public ActionResult CreateCampaign()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult CreateCampaign(Campaign EntityCampaign)
        {
            EntityCampaign.CreatedDate = DateTime.Now;
            EntityCampaign.IsActive = true;
            db.Campaigns.Add(EntityCampaign);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditCampaign(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }            
            var CampaignData = db.Campaigns.Where(m => m.Id == id).FirstOrDefault();
            ViewBag.CampaignId = CampaignData.Id;            
            if (CampaignData == null)
            {
                return HttpNotFound();
            }
            return View(CampaignData);
        }

        [HttpPost]
        public ActionResult EditCampaign(Campaign EntityCampaign)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(EntityCampaign).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult DeleteCampaign(int? id)
        {
            if (id != null)
            {
                var DelCampaign = db.Campaigns.Single(s => s.Id == id);              
                DelCampaign.IsActive = false;
            }
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllCampaign()
        {
            try
            {
                var getAllCampaign = db.Campaigns.Where(m => m.IsActive == true).OrderByDescending(m => m.CreatedDate).ToList();
                //var getAllUsers = db.users.OrderBy(x => x.CreatedOn).ToList();
                // var getAllCampaign = db.Campaigns.ToList();
                // var getAllUsers = db.users.ToList();
                if (getAllCampaign.Count() > 0)
                {
                    ViewBag.RecordExists = "true";
                }
                return Json(getAllCampaign, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}