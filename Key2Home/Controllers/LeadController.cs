﻿using Key2Home.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Key2Home.ViewModel;

namespace Key2Home.Controllers
{
    public class LeadController : Controller
    {
        KeyToHomeEntities db = new KeyToHomeEntities();
        // GET: Lead
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddLead()
        {
            ViewModelLeadDistribution _obj = new ViewModelLeadDistribution();
            _obj._listAllAdmin = new List<admin>();
            var AllAdmin = db.admins.ToList();
            foreach (var item in AllAdmin)
            {
                _obj._listAllAdmin.Add(item);
            }
            return View(_obj);
        }

        [HttpPost]
        public ActionResult AddLead(ViewModelLeadDistribution AddLeadDistribution)
        {
            LeadDistribution _obj = new LeadDistribution();       
            if(AddLeadDistribution.LeadType == "1")
            {
                _obj.LeadType = "Buyer";
            }
            if (AddLeadDistribution.LeadType == "2")
            {
                _obj.LeadType = "Seller";
            }
            if (AddLeadDistribution.LeadType == "3")
            {
                _obj.LeadType = "Buyer and Seller";
            }
            _obj.Title = AddLeadDistribution.Title;         
            _obj.PriceMin = AddLeadDistribution.PriceMin;
            _obj.PriceMax = AddLeadDistribution.PriceMax;
            _obj.Locations = AddLeadDistribution.Locations;
            _obj.IsActive= AddLeadDistribution.IsActive;    
            db.LeadDistributions.Add(_obj);
            db.SaveChanges();
            if (AddLeadDistribution.ActiveDays != null)
            {
                var ActiveDayId = AddLeadDistribution.ActiveDays.Split(',');
                LeadActiveDay _objLeadAdctiveDays = new LeadActiveDay();
                for (int ik = 0; ik < ActiveDayId.Count(); ik++)
                {
                    _objLeadAdctiveDays.LeadId = _obj.LeadId;
                    _objLeadAdctiveDays.DayId = Convert.ToInt32(ActiveDayId[ik]);
                    db.LeadActiveDays.Add(_objLeadAdctiveDays);
                    db.SaveChanges();
                }
            }

            var LoanOffId = AddLeadDistribution.LoanOffcierIds.Split(',');
            var qunat = AddLeadDistribution.LoanOffcierQuanity.Split(',');
            var division = AddLeadDistribution.LoanOffcierDivision.Split(',');
           
            if (LoanOffId!=null)
            {
                for (int i = 0; i < LoanOffId.Count(); i++)
                {
                    LeadLoanOfficier _objLoanOfficer = new LeadLoanOfficier();
                    _objLoanOfficer.LeadId = _obj.LeadId;
                    _objLoanOfficer.LoanOfficierId = Convert.ToInt32(LoanOffId[i]);
                    if (qunat != null)
                    {
                        _objLoanOfficer.Quantity = Convert.ToInt32(qunat[i]);                       
                    }               
                    if (division != null)
                    {
                        var finalDivision = division[i].Replace(@"%", string.Empty);
                        _objLoanOfficer.Division = Convert.ToInt32(finalDivision);
                    }
                    _objLoanOfficer.IsActive = true;
                    db.LeadLoanOfficiers.Add(_objLoanOfficer); 
                    db.SaveChanges();
                }
            }
            var SalesOffId = AddLeadDistribution.SaleOffcierIds.Split(',');
            var Salequnat = AddLeadDistribution.SaleOffcierQuanity.Split(',');
            var Saledivision = AddLeadDistribution.LoanOffcierDivision.Split(',');
            if (SalesOffId!= null)
            {
               
                for (int j = 0; j < LoanOffId.Count(); j++)
                {               
                    LeadSaleOfficier _objLoanOfficer = new LeadSaleOfficier();
                    _objLoanOfficer.LeadId = _obj.LeadId;

                    _objLoanOfficer.SaleOfficierId = Convert.ToInt32(LoanOffId[j]);
                    if (Salequnat != null)
                    {
                        _objLoanOfficer.Quantity = Convert.ToInt32(Salequnat[j]);
                    }
                    if (Saledivision != null)
                    {
                        var finalDivisionSale = Saledivision[j].Replace(@"%", string.Empty);
                        _objLoanOfficer.Division = Convert.ToInt32(finalDivisionSale);
                    }               
                    _objLoanOfficer.IsActive = true;
                    db.LeadSaleOfficiers.Add(_objLoanOfficer);
                    db.SaveChanges();
                }
            }
            ModelState.Clear();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLoanOffficiers(string SelAdminId)
        {
            try
            {
                ViewModelLeadDistribution obj = new ViewModelLeadDistribution();          
                if (SelAdminId != null)
                {
                    var serverIDs = SelAdminId.Split(',');
                    List<string> result = SelAdminId.Split(',').ToList();
                    foreach (var DeletedUserId in serverIDs)
                    {
                        var SelId = Convert.ToInt32(DeletedUserId);                  
                        var AllAdmin = db.admins.Where(m => m.AdminID == SelId).ToList();
                     
                        foreach (var item in AllAdmin)
                        {
                            obj._listAllAdmin.Add(item);
                        }               
                    }                    
                    obj.AdminCount = result.Count();
                }
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("No Food", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetIsaAssigments(string SelIsaId)
        {
            try
            {
                ViewModelLeadDistribution obj = new ViewModelLeadDistribution();              
                if (SelIsaId != null)
                {
                    var IDs = SelIsaId.Split(',');
                    List<string> Adminresult = SelIsaId.Split(',').ToList();
                    foreach (var DeletedUserId in IDs)
                    {
                        var SelId = Convert.ToInt32(DeletedUserId);                      
                        var AllAdmin = db.admins.Where(m => m.AdminID == SelId).ToList();
                        foreach (var item in AllAdmin)
                        {
                            obj._listAllAdmin.Add(item);
                        }
                        obj.AdminIsaCount = Adminresult.Count();
                    }
                }
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("No Ids", JsonRequestBehavior.AllowGet);
            }
        }
    }
}