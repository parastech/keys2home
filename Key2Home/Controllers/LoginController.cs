﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Key2Home.Models;
using Key2Home.ViewModel;
using System.Web.Security;
using System.Net.Mail;
using System.Net;
using Key2Home.Common;
using System.Data.Entity;

namespace Key2Home.Controllers
{
    public class LoginController : Controller
    {
        KeyToHomeEntities db = new KeyToHomeEntities();

        #region UserLogin
        public ActionResult Login()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Login(AdminLoginViewModel AdminLoginViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrEmpty(AdminLoginViewModel.Password))
                    {
                       // AdminLoginViewModel.Password = HelperMethods.Encrypt(AdminLoginViewModel.Password);
                        var LoginAdmin = db.admins.Where(m => m.AdminEmail.ToLower() == AdminLoginViewModel.EmailId.ToLower() && m.AdminPassword == AdminLoginViewModel.Password).SingleOrDefault();
                        if (LoginAdmin != null)
                        {
                            FormsAuthentication.SetAuthCookie(AdminLoginViewModel.EmailId, AdminLoginViewModel.RememberMe);
                            Session["AdminId"] = LoginAdmin.AdminID;
                            Session["EmailId"] = LoginAdmin.AdminEmail;

                            if (LoginAdmin.Type== "SuperAdmin")
                            {
                                return RedirectToAction("ActiveCustomer", "SuperAdmin");
                            }
                            if (AdminLoginViewModel.RememberMe)
                            {
                                HttpCookie cookie = new HttpCookie("AdminLogin");
                                cookie.Values.Add("AdminId", LoginAdmin.AdminID.ToString());
                                cookie.Values.Add("EmailId", LoginAdmin.AdminEmail);
                              //  string Password = HelperMethods.Decrypt(LoginAdmin.AdminPassword);
                                cookie.Values.Add("Password", LoginAdmin.AdminPassword);
                                cookie.Expires = DateTime.Now.AddDays(7);
                                Response.Cookies.Add(cookie);
                            }
                            else
                            {
                                HttpCookie myCookie = new HttpCookie("AdminLogin");
                                myCookie.Expires = DateTime.Now.AddDays(-1d);
                                Response.Cookies.Add(myCookie);
                            }
                            return RedirectToAction("Index", "User");
                        }
                        else
                        {
                            ModelState.AddModelError("Password", "Password doesn't match");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            try
            {
                Session.Clear();
                Session.Abandon();
                Session.Remove("AdminId");
                return RedirectToAction("Login");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(LostPasswordModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var CheckExists = db.admins.SingleOrDefault(m => m.AdminEmail.ToLower() == model.Email.ToLower());
                    if (CheckExists != null)
                    {
                      string password = CheckExists.AdminPassword;
                        if (!string.IsNullOrEmpty(password))
                        {
                            string admin_email = "admin@trackeveryone.com";
                            string admin_emailpwd = "Admin@123";
                            string to = CheckExists.AdminEmail;
                            MailMessage mail = new MailMessage();
                            mail.To.Add(to);
                            mail.From = new MailAddress(admin_email);
                            mail.Subject = "Recovering Password";
                            string body = "Hello " + CheckExists.AdminName + ",";
                            body += "<br /><br />Your Password is : " + password;
                            body += "<br /><br />Thanks";
                            mail.Body = body;                      
                            mail.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "ws156.win.arvixe.com";
                            //smtp.Port = 587;
                            smtp.Port = 26;
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential(admin_email, admin_emailpwd);
                            smtp.EnableSsl = false;
                            smtp.Send(mail);
                            ViewBag.result = "Password has been sent to your E-mail ID";
                            return View();
                        }
                    }
                    ViewBag.error = "Email Id does not exists";
                }
            }
            catch (Exception ex)
            {
                return View(model);
            }
            return View();
        }
        private object createEmailBody(Guid guid, DateTime resetToken, string emailId, string v)
        {
            throw new NotImplementedException();
        }
        [AllowAnonymous]
        public ActionResult ResetPassword(string email)
        {
            try
            {
                if (Session["AdminId"] != null)
                {
                    ResetPasswordModel resetmodel = new ResetPasswordModel();
                    resetmodel.Email = email;
                    return View(resetmodel);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Login", "Login");
        }
        public ActionResult ChangePassword(admin adminChangePswrdEntity)
        {
            try
            {
                if (Session["AdminId"] != null)
                {
                    int id = Convert.ToInt32(Session["AdminId"]);
                    ViewBag.Results = db.admins.Where(p => p.AdminID == id);
                    return View();
                }
            }
            catch (Exception)
            {
                throw;
            }
             return RedirectToAction("Login", "Login");
        }
        [HttpPost]
        public ActionResult ChangePassword(admin adminEntity, ChangePasswordViewModel loginVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string SessionData = Session["AdminId"].ToString();
                    if (SessionData != null)
                    {
                        adminEntity.AdminEmail = Session["EmailId"].ToString().ToLower();
                        loginVm.OldPassword = loginVm.OldPassword;
                        var data = db.admins.SingleOrDefault(p => p.AdminPassword == loginVm.OldPassword && p.AdminEmail == adminEntity.AdminEmail);
                        if (data != null)
                        {
                            data.AdminPassword = loginVm.NewPassword;
                            db.Entry(data).State = EntityState.Modified;
                            Session["AdminId"] = adminEntity.AdminID;
                            Session["EmailId"] = adminEntity.AdminEmail;
                            HttpCookie cookie = new HttpCookie("AdminLogin");
                            string NewPassword = loginVm.NewPassword;
                            cookie.Values.Add("Password", NewPassword);
                            cookie.Expires = DateTime.Now.AddDays(15);
                            Response.Cookies.Add(cookie);
                            db.SaveChanges();
                            ModelState.Clear();
                            ViewBag.result = "Password changed successfully!";
                        }
                        else
                        {
                            ModelState.Clear();
                            ViewBag.Message = "Old password is not correct";
                        }
                    }
                    return RedirectToAction("Login");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        #endregion UserLogin
    }
}