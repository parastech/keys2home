﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Key2Home.Models;
using Key2Home.SuperAdminViewModel;
using Key2Home.ViewModel;

namespace Key2Home.Controllers
{
    public class SuperAdminController : Controller
    {
        KeyToHomeEntities db = new KeyToHomeEntities();
        // GET: SuperAdmin
        public ActionResult Index()
        {
            return View();
        }

        #region SuperAdmin
        // GET: SuperAdmin
        /// <summary>
        /// Active Customer for show the view
        /// </summary>
        /// <returns></returns>
        public ActionResult ActiveCustomer()
        {

            return View("ActiveCustomerSuperAdmin");
        }
        /// <summary>
        /// Get All the Active Customer List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetActiveCustomerList()
        {
            try
            {
                using (var _context = new KeyToHomeEntities())
                {
                    int adminId = Convert.ToInt32(Session["AdminId"].ToString());
                    var obj = _context.admins.Where(p => p.AdminID != adminId  && p.IsDeleted != true).OrderByDescending(c => c.CreateOn).ToList();
                    if (obj.Count() > 0)
                    {
                        ViewBag.UserLlist = "true";

                    }
                    return Json(obj, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Agent Detail SuperAdmin for single individual
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AgentDetailSuperAdmin(int? id)
        {
            try
            {
                AppListViewModel result = new AppListViewModel();
                var obj = db.admins.Find(id);
                if (obj != null)
                {
                    result.AdminID = obj.AdminID;
                    result.AdminName = obj.AdminName;
                    result.AdminEmail = obj.AdminEmail;
                    result.Phone = obj.Phone;
                    result.Plan = obj.PLans;
                    var appObj = db.Applists.FirstOrDefault();
                    if (appObj != null)
                    {
                        result.AppLogo = appObj.AppLogo;
                        result.AppName = appObj.AppName;
                        result.AgentName = appObj.AgentName;
                        result.AppShortDiscription = appObj.AppShortDiscription;
                        result.AppFullDiscription = appObj.AppFullDiscription;
                        result.ThemeColorCode = appObj.ThemeColorCode;
                    }
                }
                return View("AgentDetailSuperAdmin", result);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Deactivate Active Customer
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DeactivateActiveCustomer(int id)
        {
            try
            {

                bool result = false;
                admin obj = new admin();
                obj = db.admins.Where(a => a.AdminID == id && a.IsDeleted != true).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsActive = (obj.IsActive == true || obj.IsActive == null) ? false : true;
                    db.SaveChanges();
                    result = true;
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }

       
        /// <summary>
        /// Delete Customer for show the view
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteCustomer()
        {

            return View("DeleteCustomerSuperAdmin");
        }
        /// <summary>
        /// Get Delete Customer List for all customer 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDeleteCustomerList()
        {
            try
            {
                using (var _context = new KeyToHomeEntities())
                {
                    int adminId = Convert.ToInt32(Session["AdminId"].ToString());
                    var obj = _context.admins.Where(c => c.AdminID != adminId && c.IsDeleted != true).OrderByDescending(o => o.CreateOn).ToList();
                    if (obj.Count() > 0)
                    {
                        ViewBag.UserLlist = "true";
                    }
                    return Json(obj, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// Get Deletet Agent Detail by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDeletetAgentDetail(int? Id)
        {
            try
            {
                admin obj = new admin();
                obj = db.admins.Find(Id);
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
        /// <summary>
        /// Delete Agent Detail Super Admin for view detail
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteAgentDetailSuperAdmin(int? id)
        {
            try
            {
                AppListViewModel result = new AppListViewModel();
                var obj = db.admins.Find(id);
                if (obj != null)
                {
                    result.AdminID = obj.AdminID;
                    result.AdminName = obj.AdminName;
                    result.AdminEmail = obj.AdminEmail;
                    result.Phone = obj.Phone;
                    result.Plan = obj.PLans;
                    var appObj = db.Applists.FirstOrDefault();
                    if (appObj != null)
                    {
                        result.AppLogo = appObj.AppLogo;
                        result.AppName = appObj.AppName;
                        result.AgentName = appObj.AgentName;
                        result.AppShortDiscription = appObj.AppShortDiscription;
                        result.AppFullDiscription = appObj.AppFullDiscription;
                        result.ThemeColorCode = appObj.ThemeColorCode;
                    }
                }
                return View("DeleteAgentDetailSuperAdmin", result);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        /// <summary>
        /// Delete User by AdminID
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DeleteUser(int id)
        {
            try
            {
                bool result = false;
                var obj = db.admins.Where(a => a.AdminID == id).FirstOrDefault();
                if (obj.AdminID > 0)
                {
                    obj.IsActive = false;
                    obj.IsDeleted = true;
                    db.SaveChanges();
                    result = true;


                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
        /// <summary>
        /// AppList for show view
        /// </summary>
        /// <returns></returns>
        public ActionResult AppList()
        {

            return View("AppListSuperAdmin");
        }
        /// <summary>
        /// App List Detail
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        public ActionResult AppListDetail()
        {
            try
            {
                using (var _context = new KeyToHomeEntities())
                {
                    var obj = _context.Applists.Where(c => c.isActive != false && c.isDeleted != true).OrderByDescending(o => o.Id).ToList();
                    if (obj.Count() > 0)
                    {
                        ViewBag.UserLlist = "true";
                    }
                    return Json(obj, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// DeleteAppList
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteAppList(int id)
        {
            try
            {
                bool result = false;
                using (var _context = new KeyToHomeEntities())
                {
                    var obj = _context.Applists.Where(c => c.Id == id).FirstOrDefault();

                    if (obj.Id > 0)
                    {
                        obj.isActive = false;
                        obj.isDeleted = true;
                        _context.SaveChanges();
                        result = true;
                    }
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult ChangePassword()
        {

            return View("ChangePasswordSuperAdmin");
        }
        /// <summary>
        /// ChangePassword
        /// </summary>
        /// <param name="adminEntity"></param>
        /// <param name="SuperVm"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePasswordSuperAdmin(ChangePasswordViewModelSuperAdmin SuperVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string SessionData = Session["AdminId"].ToString();
                    if (SessionData != null)
                    {
                        int adminId = Convert.ToInt32(SessionData);
                        var data = db.admins.SingleOrDefault(p => p.AdminID == adminId && p.AdminPassword == SuperVm.OldPassword);
                        if (data != null)
                        {
                            data.AdminPassword = SuperVm.NewPassword;
                            db.Entry(data).State = EntityState.Modified;
                            HttpCookie cookie = new HttpCookie("AdminLogin");
                            string NewPassword = SuperVm.NewPassword;
                            cookie.Values.Add("Password", NewPassword);
                            cookie.Expires = DateTime.Now.AddDays(15);
                            Response.Cookies.Add(cookie);
                            db.SaveChanges();
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("false", JsonRequestBehavior.AllowGet);
                        }
                    }
                    return RedirectToAction("Login", "Login");
                }
                return Json("ModelError", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult Logout()
        {

            return View("LogoutSuperAdmin");
        }

        #endregion SuperAdmin
    }
}