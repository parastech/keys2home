﻿using Key2Home.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Key2Home.Controllers
{
    public class TemplateController : Controller
    {
        KeyToHomeEntities db = new KeyToHomeEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Template EntityTemplate)
        {
            try
            {
                EntityTemplate.CreatedDate = DateTime.Now;
                EntityTemplate.IsActive = true;                           
                string TempName = EntityTemplate.Name;
                EntityTemplate.Description = Server.HtmlDecode(EntityTemplate.Description);
                string fileName = String.Concat(TempName, ".html");
                //System.IO.File.WriteAllText(@"~/EmailTemplates\" + fileName, EntityTemplate.Description);
                string path = Server.MapPath("~/EmailTemplates/"+ fileName);
                System.IO.File.WriteAllText(path, EntityTemplate.Description);
                EntityTemplate.TemplatePath = path;
                //string path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(@"~/EmailTemplates"), imagefileName);
                db.Templates.Add(EntityTemplate);
                db.SaveChanges();    
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }   

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var TemplateData = db.Templates.Where(m => m.TemplateId == id).FirstOrDefault();
            TemplateData.Description = Server.HtmlDecode(TemplateData.Description);
            ViewBag.TemplateId = TemplateData.TemplateId;
            if (TemplateData == null)
            {
                return HttpNotFound();
            }
            return View(TemplateData);
        }
        
        [HttpPost]
        public ActionResult Edit(Template EditTemplate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(EditTemplate).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

 
        public JsonResult Delete(int? id)
        {
            if (id != null)
            {
                var Deltemplate = db.Templates.Single(s => s.TemplateId == id);           
                Deltemplate.IsActive = false;
            }
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllTemplate()
        {
            try
            {
                var getAllTemplate = db.Templates.Where(m => m.IsActive == true).OrderByDescending(m => m.CreatedDate).ToList();             
                if (getAllTemplate.Count() > 0) 
                {
                    ViewBag.RecordExists = "true";
                }
                return Json(getAllTemplate, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
