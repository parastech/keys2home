﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Key2Home.Models;
using Key2Home.ViewModel;

namespace Key2Home.Controllers
{
    public class UserController : Controller
    {
        KeyToHomeEntities db = new KeyToHomeEntities();
        // GET: User
        public string MailTextToSend;
        #region User
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        public ActionResult Index()
        {
            try
            {
                var count = db.users.Count(b => b.status == "New");
                ViewBag.StatusCount = count;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }
        /// <summary>
        /// GetAllUsers
        /// </summary>
        /// For find list
        /// <returns></returns>

        public ActionResult GetAllUsers()
        {
            try
            {
                // var ss =  db.users.OrderByDescending((m =>m.CreatedOn).ToList();
                //var getAllUsers = db.users.OrderBy(x => x.CreatedOn).ToList();
                var getAllUsers = db.users.OrderByDescending(i => i.CreatedOn).ToList();
                // var getAllUsers = db.users.Where(m => m.status == "New").ToList();
                // var getAllUsers = db.users.ToList();
                if (getAllUsers.Count() > 0)
                {
                    ViewBag.RecordExists = "true";
                }
                return Json(getAllUsers, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult CreateUser()
        {
            var list = new List<SelectListItem>
        {
        new SelectListItem{ Text="New"},
        new SelectListItem{ Text="Unknown"},
        new SelectListItem{ Text="Cold" },
        new SelectListItem{ Text="Nurture"},
        new SelectListItem{ Text="Hot" },
        new SelectListItem{ Text="Appointment Set"},
        new SelectListItem{ Text="Met in Person -UnLikely"},
        new SelectListItem{ Text="Met in Person -Likely"},
        new SelectListItem{ Text="Agreement Signed"},
        new SelectListItem{ Text="Active Listing"},
        new SelectListItem{ Text="Under Contract"},
        new SelectListItem{ Text="Closed"},
        new SelectListItem{ Text="Inactive"},
        new SelectListItem{ Text="Do not Contact"},
        };
            ViewData["statusList"] = list;
            ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(user EntityUser, HttpPostedFileBase fileS)
        {
            var AllStatuslist = new List<SelectListItem>
        {
        new SelectListItem{ Text="New"},
        new SelectListItem{ Text="Unknown"},
        new SelectListItem{ Text="Cold" },
        new SelectListItem{ Text="Nurture"},
        new SelectListItem{ Text="Hot" },
        new SelectListItem{ Text="Appointment Set"},
        new SelectListItem{ Text="Met in Person -UnLikely"},
        new SelectListItem{ Text="Met in Person -Likely"},
        new SelectListItem{ Text="Agreement Signed"},
        new SelectListItem{ Text="Active Listing"},
        new SelectListItem{ Text="Under Contract"},
        new SelectListItem{ Text="Closed"},
        new SelectListItem{ Text="Inactive"},
        new SelectListItem{ Text="Do not Contact"},
        };
            try
            {
                if (ModelState.IsValid)
                {
                    //var CheckExists = db.RegistrationDetails.SingleOrDefault(m => m.Email.ToLower() == OBJ_LoginClass.Email.ToLower());                    
                    var CheckEmailExist = db.users.SingleOrDefault(m => m.Email.ToLower() == EntityUser.Email.ToLower());
                    if (CheckEmailExist == null)
                    {
                        string imagefileName = string.Empty;
                        if (fileS != null && fileS.ContentLength > 0)
                        {
                            imagefileName = Path.GetFileName(fileS.FileName);
                            string Extension = Path.GetExtension(imagefileName);

                            string ImageName = System.IO.Path.GetFileName(fileS.FileName);
                            string PhysicalPath = Server.MapPath("/Key2HomeImages/" + ImageName);
                            fileS.SaveAs(PhysicalPath);
                            EntityUser.pic = imagefileName;
                            // file.SaveAs(path);
                        }
                        if (EntityUser.SendWelcomeEmail == true)
                        {
                            string admin_email = "admin@trackeveryone.com";
                            string admin_emailpwd = "Admin@123";
                            MailMessage mail = new MailMessage();
                            mail.To.Add(EntityUser.Email);
                            mail.From = new MailAddress(admin_email);
                            mail.Subject = "Welcome Email";
                            // string TemplateFilePath = ConfigurationManager.AppSettings["TemplateFilePath"];
                            string FilePath = Server.MapPath(@"~/EmailTemplates/Welcome.html");
                            StreamReader str = new StreamReader(FilePath);
                            string MailText = str.ReadToEnd();
                            str.Close();
                            string body = MailText.ToString().Replace("newusername", EntityUser.FirstName + " " + EntityUser.LastName);
                            mail.Body = body;
                            mail.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "ws156.win.arvixe.com";
                            smtp.Port = 26;
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = new System.Net.NetworkCredential(admin_email, admin_emailpwd);
                            smtp.EnableSsl = false;
                            smtp.Send(mail);
                        }
                        EntityUser.CreatedOn = DateTime.Now;
                        EntityUser.IsActive = true;
                        db.users.Add(EntityUser);
                        db.SaveChanges();
                        ModelState.Clear();
                        //TempData["notice"] = "Successfully registered";
                        ViewBag.Result = "Registered successfully!";

                        ViewData["statusList"] = AllStatuslist;
                        ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.Error = "Email already exists!";
                    }
                    ViewData["statusList"] = AllStatuslist;
                    ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
                    return View(EntityUser);
                }
                //else                                          
                //{
                //    ViewBag.Error = "Enter all details!";
                //}
                ViewData["statusList"] = AllStatuslist;
                ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
                return View(EntityUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult EditUser(int? id, user EntityUser)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var AllStatuslist = new List<SelectListItem>
        {
           new SelectListItem{ Text="New"},
        new SelectListItem{ Text="Unknown"},
        new SelectListItem{ Text="Cold" },
        new SelectListItem{ Text="Nurture"},
        new SelectListItem{ Text="Hot" },
        new SelectListItem{ Text="Appointment Set"},
         new SelectListItem{ Text="Met in Person -UnLikely"},
          new SelectListItem{ Text="Met in Person -Likely"},
          new SelectListItem{ Text="Agreement Signed"},
           new SelectListItem{ Text="Active Listing"},
                new SelectListItem{ Text="Under Contract"},
        new SelectListItem{ Text="Closed"},
        new SelectListItem{ Text="Inactive"},
        new SelectListItem{ Text="Do not Contact"},
        };
            //user tbl_Users = db.users.Find(id);
            var UserData = db.users.Where(m => m.UserID == id).FirstOrDefault();
            ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
            ViewBag.UserId = UserData.UserID;
            ViewData["statusList"] = AllStatuslist;
            if (UserData != null)
            {
                if (UserData.SendWelcomeEmail)
                    ViewBag.CheckedType = "true";
                else
                    ViewBag.CheckedType = "false";
            }
            if (UserData == null)
            {
                return HttpNotFound();
            }

            return View(UserData);
        }

 
        [HttpPost]
        public ActionResult EditUser(user EditUser, HttpPostedFileBase fileS)
        {
            try
            {
                var data = db.users.Where(m => m.UserID == EditUser.UserID).FirstOrDefault();
                if (ModelState.IsValid)
                {
                    if (fileS == null)
                    {
                        EditUser.pic = data.pic;
                    }
                    string imagefileName = string.Empty;
                    if (fileS != null && fileS.ContentLength > 0)
                    {
                        imagefileName = Path.GetFileName(fileS.FileName);
                        string Extension = Path.GetExtension(imagefileName);
                        string ImageName = System.IO.Path.GetFileName(fileS.FileName);
                        string PhysicalPath = Server.MapPath("/Key2HomeImages/" + ImageName);
                        fileS.SaveAs(PhysicalPath);
                        EditUser.pic = imagefileName;
                    }
                    // data.UserID = EditUser.UserID;
                    data.FirstName = EditUser.FirstName;
                    data.LastName = EditUser.LastName;
                    data.CreatedOn = EditUser.CreatedOn;
                    data.DOB = EditUser.DOB;
                    data.Email = EditUser.Email;
                    data.Password = EditUser.Password;
                    data.Phone1 = EditUser.Phone1;
                    data.Phone2 = EditUser.Phone2;
                    data.TeamXID = EditUser.TeamXID;
                    data.Notes = EditUser.Notes;
                    data.Warnings = EditUser.Warnings;
                    data.SendWelcomeEmail = EditUser.SendWelcomeEmail;
                    data.LastLogin = EditUser.LastLogin;
                    data.SavSearch = EditUser.SavSearch;
                    data.roles = EditUser.roles;
                    data.status = EditUser.status;
                    data.pic = EditUser.pic;
                    data.AdminXID = EditUser.AdminXID;
                    data.Location = EditUser.Location;
                    data.Mode = EditUser.Mode;
                    data.ID = EditUser.ID;
                    data.IsActive = EditUser.IsActive;
                    data.LoanManagerXID = EditUser.LoanManagerXID;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        public ActionResult UserDetails(int id)
        {
            //var model = db.users.Where(m => m.UserID == id).FirstOrDefault();
            user frnds = new user();
            frnds = db.users.Find(id);
            return PartialView("_UserDetails", frnds);
            //  return PartialView(model);
        }

        public ActionResult GetDetailOfUser(Int32 UserId)
        {
            try
            {
                var getAllUser = db.users.Where(m => m.UserID == UserId).ToList();
                if (getAllUser.Count() > 0)
                {
                    ViewBag.RecordExists = "true";
                }
                return Json(getAllUser, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Users(int? sortId, int? DateId)
        {
            try
            {
                ViewModelShowAllUsers obj = new ViewModelShowAllUsers();
                if (sortId == 1)
                {

                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(m => m.IsActive == true).OrderBy(m => m.FirstName).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }
                }
                else if (sortId == 2)
                {

                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(m => m.IsActive == true).OrderBy(m => m.status).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }


                }
                else if (sortId == 3)
                {

                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(m => m.IsActive == true).OrderBy(m => m.CreatedOn).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }


                }
                else if (sortId == 4)
                {

                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(m => m.IsActive == true).OrderBy(m => m.LastLogin).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }


                }
                else
                {
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(m => m.IsActive == true).OrderBy(m => m.CreatedOn).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }

                }
                if (DateId == 1)
                {
                    var dt = DateTime.Now.AddDays(-1);
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(x => x.CreatedOn > dt).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }

                }
                else if (DateId == 2)
                {
                    var dt = DateTime.Now.AddDays(-7);
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(x => x.CreatedOn > dt).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }

                }
                else if (DateId == 3)
                {
                    var dt = DateTime.Now.AddDays(-30);
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(x => x.CreatedOn > dt).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }

                }
                else if (DateId == 4)
                {
                    var dt = DateTime.Now.AddDays(-90);
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(x => x.CreatedOn > dt).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }

                }
                else if (DateId == 5)
                {
                    var dt = DateTime.Now.AddDays(-180);
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(x => x.CreatedOn > dt).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }

                }
                else if (DateId == 6)
                {
                    var dt = DateTime.Now.AddDays(-365);
                    obj._listAllUsers = new List<user>();
                    var ShowAllUser = db.users.Where(x => x.CreatedOn > dt).ToList();
                    if (ShowAllUser.Count() > 0)
                    {
                        foreach (var item in ShowAllUser)
                        {
                            obj._listAllUsers.Add(item);
                        }
                        ViewBag.RecordExists = "true";
                    }
                }
                return View(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public ActionResult Users()
        //{
        //    try
        //    {
        //        ViewModelShowAllUsers obj = new ViewModelShowAllUsers();
        //        obj._listAllUsers = new List<user>();
        //       //  var ShowAllUser = db.users.Where(m => m.IsActive == true).ToList();
        //       // var ShowAllUser = db.users.OrderByDescending((i => i.CreatedOn) && (i=>i.IsActive == true)).ToList();
        //        var ShowAllUser = db.users.Where(m => m.IsActive == true).OrderByDescending(m => m.CreatedOn).ToList();
        //        if (ShowAllUser.Count() > 0)
        //        {
        //            foreach (var item in ShowAllUser)
        //            {
        //                obj._listAllUsers.Add(item);
        //            }
        //            ViewBag.RecordExists = "true";
        //        }
        //        return View(obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public JsonResult DeleteSelUser(string id)
        {
            var serverIDs = id.Split(',');
            foreach (var DeletedUserId in serverIDs)
            {
                var SelId = Convert.ToInt32(DeletedUserId);
                var student = db.users.Single(s => s.UserID == SelId);
                //.users.Remove(student);
                student.IsActive = false;
            }
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSelMessage(string id)
        {
            var serverIDs = id.Split(',');

            foreach (var DeletedUserId in serverIDs)
            {
                var SelId = Convert.ToInt32(DeletedUserId);
                var student = db.TextMessages.Single(s => s.Id == SelId);
                db.TextMessages.Remove(student);
                // student.IsActive = false;
            }
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSelSingleMessage(int? id)
        {
            // var serverIDs = id.Split(',');
            // foreach (var DeletedUserId in serverIDs)
            //{
            // var SelId = Convert.ToInt32(id);
            var student = db.TextMessages.Single(s => s.Id == id);
            db.TextMessages.Remove(student);
            // student.IsActive = false;
            // }
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public void GetTemplateValues(int id, out string PathName, out string TemplateName)
        {
            var query = db.Templates.SingleOrDefault(u => u.TemplateId == id);
            PathName = query.TemplatePath;
            TemplateName = query.Name;
        }

        public JsonResult AddTemplate(int? TemplateID)
        {
            string PathName;
            string TemplateName;
            var query = db.Templates.SingleOrDefault(u => u.TemplateId == TemplateID);
            PathName = query.TemplatePath;
            TemplateName = query.Name;
            //inputSubject.Value = TemplateName;
            //  string TemplateFilePath = ConfigurationManager.AppSettings["TemplateFilePath"]+ PathName;
            //string TemplateFilePath = Server.MapPath(@"~/EmailTemplates/") + PathName;
            // string TemplateFilePath = ConfigurationManager.AppSettings["TemplateFilePath"] + PathName;
            //Fetching Email Body Text from EmailTemplate File.
            //   string FilePath = TemplateFilePath;
            StreamReader str = new StreamReader(PathName);
            MailTextToSend = str.ReadToEnd();
            str.Close();
            return Json(MailTextToSend, JsonRequestBehavior.AllowGet);
            //  return RedirectToAction("")
            // return Redirect(returnUrl);
        }

        public ActionResult SendMassEmail(string SelectedUser, ViewModelSendMassMail viewModelSendMassMail)
        {
            ViewModelSendMassMail obj = new ViewModelSendMassMail();
            obj._listAllUsersEmails = new List<user>();
            var ShowAllUser = db.users.ToList();
            var AllTemplates = db.Templates.ToList();
            if (SelectedUser != null)
            {
                string[] tokens = SelectedUser.ToString().Split(',');
                var count = tokens.Count();
                foreach (var ID in tokens)
                {
                    if (ID != null)
                    {
                        var Userid = Convert.ToInt32(ID);
                        var GetEmail = db.users.SingleOrDefault(u => u.UserID == Userid);
                        //foreach (var item in ShowAllUser)
                        //  {
                        // obj._listAllUserEmail.Add(item);
                        var Emails = db.users.Where(a => a.UserID == Userid).FirstOrDefault();
                        // var UserMail = Emails.Email;
                        obj._listAllUsersEmails.Add(Emails);
                        // }
                    }
                }

                obj.UserCount = String.Concat(count, "Leads");
                obj.selUser = SelectedUser;
            }
            obj._listAllTemplates = AllTemplates;
            // return Json(new { result = "success", url = Url.Action("SendMassEmail", "User") });
            return PartialView("SendMassEmail", obj);
        }

        public ActionResult MassEmail()
        {
            var Users = Request.Form["selUser"];
            if (Users != null)
            {
                string[] tokens = Users.ToString().Split(',');
                foreach (var ID in tokens)
                {
                    if (ID != null)
                    {
                        var Userid = Convert.ToInt32(ID);
                        var UserData = db.users.Where(m => m.UserID == Userid).FirstOrDefault();
                        string emailSender = ConfigurationManager.AppSettings["emailsender"].ToString();
                        string emailSenderPassword = ConfigurationManager.AppSettings["password"].ToString();
                        string emailSenderHost = ConfigurationManager.AppSettings["smtpserver"].ToString();
                        int emailSenderPort = Convert.ToInt16(ConfigurationManager.AppSettings["portnumber"]);
                        Boolean emailIsSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"]);
                        //  Repalce [newusername] = signup user name                
                        string subject = Request.Form["Subject"];
                        //Base class for sending email
                        MailMessage _mailmsg = new MailMessage();
                        //Make TRUE because our body text is html
                        _mailmsg.IsBodyHtml = true;
                        //Set From Email ID
                        _mailmsg.From = new MailAddress(emailSender);
                        //Set To Email ID
                        //  _mailmsg.To.Add("anupambedi.parastechnologies@gmail.com");
                        var GetEmail = db.users.SingleOrDefault(u => u.UserID == Userid);
                        _mailmsg.To.Add(GetEmail.Email);
                        //Set Subject                
                        _mailmsg.Subject = subject;
                        //Set Body Text of Email 
                        string Html = Request.Form["Body"];
                        string DecodedHtml = Server.HtmlDecode(Html);
                        string MailText = DecodedHtml.Replace("[newusername]", UserData.FirstName + UserData.LastName.Trim());
                        _mailmsg.Body = MailText;
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            var UploadedFile = files.Get(i);
                            //Checking file is available to save.  
                            if (UploadedFile != null)
                            {
                                var InputFileName = Path.GetFileName(UploadedFile.FileName);
                                _mailmsg.Attachments.Add(new Attachment(UploadedFile.InputStream, InputFileName));

                            }
                        }
                        //Now set your SMTP 
                        SmtpClient _smtp = new SmtpClient();
                        //Set HOST server SMTP detail
                        _smtp.Host = emailSenderHost;
                        //Set PORT number of SMTP
                        _smtp.Port = emailSenderPort;
                        //Set SSL --> True / False
                        _smtp.EnableSsl = emailIsSSL;
                        //Set Sender UserEmailID, Password
                        System.Net.NetworkCredential _network = new NetworkCredential(emailSender, emailSenderPassword);
                        _smtp.Credentials = _network;
                        //Send Method will send your MailMessage create above.
                        _smtp.Send(_mailmsg);
                    }
                }
            }
            // return Json("Success", JsonRequestBehavior.AllowGet);
            return RedirectToAction("Users");
        }

        public ActionResult LiveActivity(int? UserId)
        {
            try
            {
                user _objModel = new user();
                var AllTextMessage = db.TextMessages.Where(m => m.UserId == UserId).ToList();
                var count = AllTextMessage.Count();
                _objModel = db.users.Find(UserId);
                var UserData = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
                ViewModelUserTextMessage _objViewModel = new ViewModelUserTextMessage();
                _objViewModel._objMessageList = new List<TextMessage>();
                _objViewModel.UserID = _objModel.UserID;
                _objViewModel.TeamXID = _objModel.TeamXID;
                _objViewModel.FirstName = _objModel.FirstName;
                _objViewModel.LastName = _objModel.LastName;
                _objViewModel.CreatedOn = _objModel.CreatedOn;
                _objViewModel.DOB = _objModel.DOB;
                _objViewModel.Email = _objModel.Email;
                _objViewModel.Phone1 = _objModel.Phone1;
                _objViewModel.Phone2 = _objModel.Phone2;
                _objViewModel.Notes = _objModel.Notes;
                _objViewModel.Warnings = _objModel.Warnings;
                _objViewModel.SendWelcomeEmail = _objModel.SendWelcomeEmail;
                _objViewModel.roles = _objModel.roles;
                _objViewModel.status = _objModel.status;
                _objViewModel.pic = _objModel.pic;
                _objViewModel.Location = _objModel.Location;
                _objViewModel.Mode = _objModel.Mode;
                //_objViewModel.Id = Convert.ToInt32(_objModel.ID);
                _objViewModel.IsActive = _objModel.IsActive;
                _objViewModel.Email = UserData.Email;
                foreach (var item in AllTextMessage)
                {
                    _objViewModel.count = count;
                    _objViewModel._objMessageList.Add(item);
                }

                return View(_objViewModel);
                //if (UserId != null)
                //{
                //    var ShowAllUser = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
                //    if (ShowAllUser != null)
                //    {
                //        //foreach (var item in ShowAllUser)
                //        // {
                //        //  obj._listAllUsers.Add(item);
                //        //}
                //        ViewBag.RecordExists = "true";
                //    }
                //    return View(ShowAllUser);
                //}
                //return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult UserContactDetails(int? UserId)
        {
            user frnds = new user();
            frnds = db.users.Find(UserId);
            return View(frnds);
        }

        public ActionResult UserEmail(int? UserId)
        {
            user frnds = new user();
            frnds = db.users.Find(UserId);
            return View(frnds);
        }

        public ActionResult ActivitiesTasks(int? UserId)
        {
            user _objModel = new user();
            var AllTextMessage = db.TextMessages.Where(m => m.UserId == UserId).ToList();
            var AllNote = db.Notes.Where(m => m.UserId == UserId).ToList();
            var AllTasks = db.Tasks.Where(m => m.UserId == UserId && m.IsCompleted == false).ToList();
            var count = AllTextMessage.Count();
            _objModel = db.users.Find(UserId);
            var UserData = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
            ViewModelUserTextMessage _objViewModel = new ViewModelUserTextMessage();
            _objViewModel._listNote = new List<Note>();
            _objViewModel._listSocial = new List<Note>();
            _objViewModel._listFace = new List<Note>();
            _objViewModel._listPhone = new List<Note>();
            _objViewModel._listEmail = new List<Note>();
            _objViewModel._listTasks = new List<Task>();
            _objViewModel._objMessageList = new List<TextMessage>();            
            _objViewModel.UserID = _objModel.UserID;
            _objViewModel.TeamXID = _objModel.TeamXID;
            _objViewModel.FirstName = _objModel.FirstName;
            _objViewModel.LastName = _objModel.LastName;
            _objViewModel.CreatedOn = _objModel.CreatedOn;
            _objViewModel.DOB = _objModel.DOB;
            _objViewModel.Email = _objModel.Email;
            _objViewModel.Phone1 = _objModel.Phone1;
            _objViewModel.Phone2 = _objModel.Phone2;
            _objViewModel.Notes = _objModel.Notes;
            _objViewModel.Warnings = _objModel.Warnings;
            _objViewModel.SendWelcomeEmail = _objModel.SendWelcomeEmail;
            _objViewModel.roles = _objModel.roles;
            _objViewModel.status = _objModel.status;
            _objViewModel.pic = _objModel.pic;
            _objViewModel.Location = _objModel.Location;
            _objViewModel.Mode = _objModel.Mode; 
            //_objViewModel.Id = Convert.ToInt32(_objModel.ID);
            _objViewModel.IsActive = _objModel.IsActive;
            _objViewModel.Email = UserData.Email;
            if (_objViewModel._listNote != null)
            {
                _objViewModel._listNote = db.Notes.Where(m => m.NoteId == 1 && m.UserId == UserId).ToList();
            }
            if (_objViewModel._listPhone != null)
            {
                _objViewModel._listPhone = db.Notes.Where(m => m.NoteId == 2 && m.UserId == UserId).ToList();
            }
            if (_objViewModel._listEmail != null)
            {
                _objViewModel._listEmail = db.Notes.Where(m => m.NoteId == 3 && m.UserId == UserId).ToList();
            }
            if (_objViewModel._listSocial != null)
            {
                _objViewModel._listSocial = db.Notes.Where(m => m.NoteId == 4 && m.UserId == UserId).ToList();
            }
            if (_objViewModel._listFace != null)
            {
                _objViewModel._listFace = db.Notes.Where(m => m.NoteId == 5 && m.UserId == UserId).ToList();
            }
            foreach (var item in AllTextMessage)
            {
                _objViewModel.count = count;
                _objViewModel._objMessageList.Add(item);
            }
            foreach (var item in AllTasks)
            {         
                _objViewModel._listTasks.Add(item);
            }
            var TaskTypelist = new List<SelectListItem>
          {
            new SelectListItem{ Text="Phone Call",Value= "1"},
            new SelectListItem{ Text="E-Mail",Value= "2"},
            new SelectListItem{ Text="Social Media",Value= "3"},
            new SelectListItem{ Text="Face to Face",Value= "4"}, 
            new SelectListItem{ Text="Text(SMS)",Value= "5"},
            new SelectListItem{ Text="Administrative",Value= "6"}
        };
            var Prioritylist = new List<SelectListItem>
          {
            new SelectListItem{ Text="Low",Value= "1"},
            new SelectListItem{ Text="High",Value= "2"},
            new SelectListItem{ Text="Critical",Value= "3"},         
        };
            ViewData["TaskTypelist"] = TaskTypelist;
            ViewData["Prioritylist"] = Prioritylist;
            //ViewBag.TaskTypelist = TaskTypelist;
           // ViewBag.Prioritylist = Prioritylist;
            ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
            return View(_objViewModel);
            //return View(db.TextMessages.Where(m => m.UserId == UserId).ToList());
        }

        public ActionResult GetAllUserMail(int? UserID)
        {
            try
            {
                var getAllUsersEmail = db.UserMails.Where(m => m.UserId == UserID).ToList();
                if (getAllUsersEmail.Count() > 0)
                {
                    foreach (var item in getAllUsersEmail)
                    {
                        item.Message = Server.HtmlDecode(item.Message);
                    }
                    ViewBag.RecordExists = "true";
                }
                return Json(getAllUsersEmail, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ComposeUserMail(int? UserId)
        {
            user _objModel = new user();

            _objModel = db.users.Find(UserId);
            var UserData = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
            ViewModelUserMail _objViewModel = new ViewModelUserMail();
            _objViewModel.UserID = _objModel.UserID;
            _objViewModel.FirstName = _objModel.FirstName;
            _objViewModel.LastName = _objModel.LastName;
            _objViewModel.CreatedOn = _objModel.CreatedOn;
            _objViewModel.DOB = _objModel.DOB;
            _objViewModel.Email = _objModel.Email;
            _objViewModel.Phone1 = _objModel.Phone1;
            _objViewModel.Phone2 = _objModel.Phone2;
            _objViewModel.Notes = _objModel.Notes;
            _objViewModel.Warnings = _objModel.Warnings;
            _objViewModel.SendWelcomeEmail = _objModel.SendWelcomeEmail;
            _objViewModel.roles = _objModel.roles;
            _objViewModel.status = _objModel.status;
            _objViewModel.pic = _objModel.pic;
            _objViewModel.Location = _objModel.Location;
            _objViewModel.Mode = _objModel.Mode;
            _objViewModel.ID = _objModel.ID;
            _objViewModel.IsActive = _objModel.IsActive;
            _objViewModel.To = UserData.Email;
            return View(_objViewModel);
        }
        [HttpPost]
        public ActionResult ComposeUserMail(ViewModelUserMail ObjViewModelUserMail, HttpPostedFileBase[] Attachment)
        {
            string emailSender = ConfigurationManager.AppSettings["emailsender"].ToString();
            string emailSenderPassword = ConfigurationManager.AppSettings["password"].ToString();
            string emailSenderHost = ConfigurationManager.AppSettings["smtpserver"].ToString();
            int emailSenderPort = Convert.ToInt16(ConfigurationManager.AppSettings["portnumber"]);
            Boolean emailIsSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSSL"]);
            //  Repalce [newusername] = signup user name                
            string subject = ObjViewModelUserMail.Subject;
            //Base class for sending email
            MailMessage _mailmsg = new MailMessage();
            //Make TRUE because our body text is html
            _mailmsg.IsBodyHtml = true;
            //Set From Email ID
            _mailmsg.From = new MailAddress(emailSender);
            _mailmsg.To.Add(ObjViewModelUserMail.To);
            //Set Subject
            _mailmsg.Subject = subject;
            _mailmsg.Body = Server.HtmlDecode(ObjViewModelUserMail.Message);
            string fileName;
            foreach (HttpPostedFileBase file in Attachment)
            {
                if (file != null)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var InputFileName = Path.GetFileName(file.FileName);
                    _mailmsg.Attachments.Add(new Attachment(file.InputStream, InputFileName));
                }
            }
            //Now set your SMTP 
            SmtpClient _smtp = new SmtpClient();
            //Set HOST server SMTP detail
            _smtp.Host = emailSenderHost;
            //Set PORT number of SMTP
            _smtp.Port = emailSenderPort;
            //Set SSL --> True / False
            _smtp.EnableSsl = emailIsSSL;
            //Set Sender UserEmailID, Password
            System.Net.NetworkCredential _network = new NetworkCredential(emailSender, emailSenderPassword);
            _smtp.Credentials = _network;
            //Send Method will send your MailMessage create above.
            _smtp.Send(_mailmsg);

            UserMail objMOdel = new UserMail();
            objMOdel.CreatedDate = Convert.ToString(DateTime.Now);
            objMOdel.Message = Server.HtmlDecode(ObjViewModelUserMail.Message);
            //objMOdel.SendFrom = ObjViewModelUserMail.;
            objMOdel.SendTo = ObjViewModelUserMail.To;
            objMOdel.UserId = ObjViewModelUserMail.UserID;
            objMOdel.Subject = subject;
            db.UserMails.Add(objMOdel);
            db.SaveChanges();
            return RedirectToAction("LiveActivity", new { UserId = ObjViewModelUserMail.UserID });
        }
        public ActionResult InboxUserMail(int? UserId)
        {
            var allInboxMail = db.UserMails.Where(m => m.UserId == UserId).ToList();
            if (allInboxMail != null)
            {
                return View(allInboxMail);
            }
            return View();
        }
        public ActionResult GetAllSentMail(int? UserID)
        {
            try
            {
                var getAllSentEmail = db.UserMails.Where(m => m.UserId == UserID).ToList();
                if (getAllSentEmail.Count() > 0)
                {
                    foreach (var item in getAllSentEmail)
                    {
                        item.Message = Server.HtmlDecode(item.Message);
                    }
                    ViewBag.RecordExists = "true";
                    return Json(getAllSentEmail, JsonRequestBehavior.AllowGet);
                }
                return Json(getAllSentEmail, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult GetAllUserCampaign(int? UserID)
        {
            try
            {
                // int NewUserId = UserId;
                var result = (from sw in db.UserCampaigns
                              join ct in db.Applists on sw.AgentId equals ct.Id
                              join cam in db.Campaigns on sw.CampaignId equals cam.Id
                              where sw.UserId == UserID
                              select new { cam.Name, sw.CreatedDate, sw.EndDate, sw.Resolution }).ToList();


                if (result.Count() > 0)
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult InboxMail(int? Id, int? UserId)
        {
            user _objModel = new user();
            _objModel = db.users.Find(UserId);
            var UserData = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
            ViewModelUserMail _objViewModel = new ViewModelUserMail();
            _objViewModel.UserID = _objModel.UserID;
            _objViewModel.FirstName = _objModel.FirstName;
            _objViewModel.LastName = _objModel.LastName;
            _objViewModel.CreatedOn = _objModel.CreatedOn;
            _objViewModel.DOB = _objModel.DOB;
            _objViewModel.Email = _objModel.Email;
            _objViewModel.Phone1 = _objModel.Phone1;
            _objViewModel.Phone2 = _objModel.Phone2;
            _objViewModel.Notes = _objModel.Notes;
            _objViewModel.Warnings = _objModel.Warnings;
            _objViewModel.SendWelcomeEmail = _objModel.SendWelcomeEmail;
            _objViewModel.roles = _objModel.roles;
            _objViewModel.status = _objModel.status;
            _objViewModel.pic = _objModel.pic;
            _objViewModel.Location = _objModel.Location;
            _objViewModel.Mode = _objModel.Mode;
            _objViewModel.ID = _objModel.ID;
            _objViewModel.IsActive = _objModel.IsActive;
            _objViewModel.To = UserData.Email;
            var allInboxMail = db.UserMails.Where(m => m.Id == Id && m.UserId == UserId).FirstOrDefault();
            _objViewModel.Subject = allInboxMail.Subject;
            _objViewModel.Message = allInboxMail.Message;
            _objViewModel.SendFrom = allInboxMail.SendFrom;
            _objViewModel.SendTo = allInboxMail.SendTo;
            _objViewModel.CreatedDate = allInboxMail.CreatedDate;
            // var query = db.tbl_hotelfaciltiestypes.Where(p => p.hotelid == hotelid && p.facilitytypeid == facilitytypeid).FirstOrDefault();
            if (allInboxMail != null)
            {
                return View(_objViewModel);
            }
            return View(_objViewModel);
            //return View();
        }
        public ActionResult SentUserMail(int? UserId)
        {
            user frnds = new user();
            frnds = db.users.Find(UserId);
            return View(frnds);
        }
        public ActionResult TextMessage(int? UserId)
        {
            user _objModel = new user();
            var AllTextMessage = db.TextMessages.Where(m => m.UserId == UserId).ToList();
            var count = db.TextMessages.Where(m => m.UserId == UserId).Count();
            _objModel = db.users.Find(UserId);
            var UserData = db.users.Where(m => m.UserID == UserId).FirstOrDefault();

            ViewModelUserTextMessage _objViewModel = new ViewModelUserTextMessage();
            _objViewModel._objMessageList = new List<TextMessage>();
            _objViewModel.UserID = _objModel.UserID;
            _objViewModel.TeamXID = _objModel.TeamXID;
            _objViewModel.FirstName = _objModel.FirstName;
            _objViewModel.LastName = _objModel.LastName;
            _objViewModel.CreatedOn = _objModel.CreatedOn;
            _objViewModel.DOB = _objModel.DOB;
            _objViewModel.Email = _objModel.Email;
            _objViewModel.Phone1 = _objModel.Phone1;
            _objViewModel.Phone2 = _objModel.Phone2;
            _objViewModel.Notes = _objModel.Notes;
            _objViewModel.Warnings = _objModel.Warnings;
            _objViewModel.SendWelcomeEmail = _objModel.SendWelcomeEmail;
            _objViewModel.roles = _objModel.roles;
            _objViewModel.status = _objModel.status;
            _objViewModel.pic = _objModel.pic;
            _objViewModel.Location = _objModel.Location;
            _objViewModel.Mode = _objModel.Mode;
            //_objViewModel.Id = Convert.ToInt32(_objModel.ID);
            _objViewModel.IsActive = _objModel.IsActive;
            _objViewModel.Email = UserData.Email;
            foreach (var item in AllTextMessage)
            {
                _objViewModel.count = count;
                _objViewModel._objMessageList.Add(item);
            }
            return View(_objViewModel);
        }
        [HttpPost]
        public JsonResult TextMessage(ViewModelUserTextMessage ViewModelAddTextMessage)
        {
            TextMessage objMOdel = new TextMessage();
            objMOdel.SentDate = Convert.ToString(DateTime.Now);
            objMOdel.Message = ViewModelAddTextMessage.Message;
            objMOdel.AgentId = ViewModelAddTextMessage.TeamXID;
            objMOdel.UserId = ViewModelAddTextMessage.UserID;
            objMOdel.UserName = ViewModelAddTextMessage.FirstName + ViewModelAddTextMessage.LastName;
            objMOdel.Mobile = ViewModelAddTextMessage.Mobile;
            db.TextMessages.Add(objMOdel);
            db.SaveChanges();
            // var id = ViewModelAddTextMessage.UserID;
            //return Json(id, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("TextMessage", new { UserId = ViewModelAddTextMessage.UserID });
            //return View();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult EditContactDetails(int? UserId)
        {
            var list = new List<SelectListItem>
        {
        new SelectListItem{ Text="New",Value= "1"},
        new SelectListItem{ Text="Unknown",Value= "2"},
        new SelectListItem{ Text="Cold",Value= "3"},
        new SelectListItem{ Text="Nurture",Value= "4"},
        new SelectListItem{ Text="Hot",Value= "5"},
        new SelectListItem{ Text="Appointment Set",Value= "6"},
        new SelectListItem{ Text="Met in Person -UnLikely",Value= "7"},
        new SelectListItem{ Text="Met in Person -Likely",Value= "8"},
        new SelectListItem{ Text="Agreement Signed",Value= "9"},
        new SelectListItem{ Text="Active Listing",Value= "10"},
        new SelectListItem{ Text="Under Contract",Value= "11"},
        new SelectListItem{ Text="Closed",Value= "12"},
        new SelectListItem{ Text="Inactive",Value= "13"},
        new SelectListItem{ Text="Do not Contact",Value= "14"}
        };
            ViewData["statusList"] = list;
            ViewBag.statusList = list;
            var LeadTypelist = new List<SelectListItem>
        {
        new SelectListItem{ Text="Buyer",Value= "1"},
        new SelectListItem{ Text="Seller",Value= "2"},
        new SelectListItem{ Text="Buyer and Seller",Value= "3"}
        };
            ViewData["LeadTypelist"] = LeadTypelist;
            ViewBag.LeadTypelist = LeadTypelist;

            user frnds = new user();
            frnds = db.users.Find(UserId);
            return View(frnds);
            //return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditContactDetails(user EditContactDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(EditContactDetails).State = EntityState.Modified;
                    if (EditContactDetails.status == "1")
                    {
                        EditContactDetails.status = "New";
                    }
                    if (EditContactDetails.status == "2")
                    {
                        EditContactDetails.status = "Unknown";
                    }
                    if (EditContactDetails.status == "3")
                    {
                        EditContactDetails.status = "Cold";
                    }
                    if (EditContactDetails.status == "4")
                    {
                        EditContactDetails.status = "Nurture";
                    }
                    if (EditContactDetails.status == "5")
                    {
                        EditContactDetails.status = "Hot";
                    }
                    if (EditContactDetails.status == "6")
                    {
                        EditContactDetails.status = "Appointment Set";
                    }
                    if (EditContactDetails.status == "7")
                    {
                        EditContactDetails.status = "Met in Person -UnLikely";
                    }
                    if (EditContactDetails.status == "8")
                    {
                        EditContactDetails.status = "Met in Person -Likely";
                    }
                    if (EditContactDetails.status == "9")
                    {
                        EditContactDetails.status = "Agreement Signed";
                    }
                    if (EditContactDetails.status == "10")
                    {
                        EditContactDetails.status = "Active Listing";
                    }
                    if (EditContactDetails.status == "11")
                    {
                        EditContactDetails.status = "Under Contract";
                    }
                    if (EditContactDetails.status == "12")
                    {
                        EditContactDetails.status = "Closed";
                    }
                    if (EditContactDetails.status == "13")
                    {
                        EditContactDetails.status = "Inactive";
                    }
                    if (EditContactDetails.status == "14")
                    {
                        EditContactDetails.status = "Do not Contact";
                    }

                    db.SaveChanges();
                    //  ViewBag.SongList = db.Songs.Where(m => m.BookId == EditAlbum.BookId);
                    ModelState.Clear();
                }
                //  return RedirectToAction("EditContactDetails");
                return RedirectToAction("UserContactDetails", new { UserId = EditContactDetails.UserID });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult UserCampaigns(int? UserId)
        {
            user _objModel = new user();

            _objModel = db.users.Find(UserId);

            ViewModelUserTextMessage _objViewModel = new ViewModelUserTextMessage();
            _objViewModel._objMessageList = new List<TextMessage>();
            _objViewModel.UserID = _objModel.UserID;
            _objViewModel.TeamXID = _objModel.TeamXID;
            _objViewModel.FirstName = _objModel.FirstName;
            _objViewModel.LastName = _objModel.LastName;
            _objViewModel.CreatedOn = _objModel.CreatedOn;
            _objViewModel.DOB = _objModel.DOB;
            _objViewModel.Email = _objModel.Email;
            _objViewModel.Phone1 = _objModel.Phone1;
            _objViewModel.Phone2 = _objModel.Phone2;
            _objViewModel.Notes = _objModel.Notes;
            _objViewModel.Warnings = _objModel.Warnings;
            _objViewModel.SendWelcomeEmail = _objModel.SendWelcomeEmail;
            _objViewModel.roles = _objModel.roles;
            _objViewModel.status = _objModel.status;
            _objViewModel.pic = _objModel.pic;
            _objViewModel.Location = _objModel.Location;
            _objViewModel.Mode = _objModel.Mode;
            _objViewModel.IsActive = _objModel.IsActive;
            ViewData["TeamAgentList"] = new SelectList(db.Applists, "Id", "AgentName");
            ViewData["CampaignList"] = new SelectList(db.Campaigns, "Id", "Name");
            return View(_objViewModel);
        }
        [HttpGet]
        public ActionResult AddUserCampaigns(int? UserID)
        {
            // Viewbag["TeamAgentList"] = new SelectList(db.Applists, "Id", "AgentName");
            ViewBag.TeamAgentList = new SelectList(db.Applists, "Id", "AgentName");
            ViewBag.CampaignList = new SelectList(db.Campaigns, "Id", "Name");
            // ViewData["CampaignList"] = new SelectList(db.Campaigns, "Id", "Name");
            return PartialView("_AddUserCampaign");
        }
        [HttpPost]
        public ActionResult AddUserCampaigns(ViewModelUserTextMessage AddUserCampaign)
        {
            UserCampaign obj = new UserCampaign();
            obj.AgentId = AddUserCampaign.AgentId;
            obj.CampaignId = AddUserCampaign.AgentId;
            obj.UserId = AddUserCampaign.UserID;
            obj.CreatedDate = Convert.ToString(DateTime.Now);
            obj.EndDate = Convert.ToString(DateTime.Now);
            db.UserCampaigns.Add(obj);
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddNote(int NoteId, string NoteDes, int UserIdentity)
        {
            try
            {
                Note obj = new Note();
                if (NoteId == 1)
                {
                    obj.NoteId = NoteId;
                    obj.NoteText = "Notes";
                    obj.Icons = "file";
                }
                if (NoteId == 2)
                {
                    obj.NoteId = NoteId;
                    obj.NoteText = "Phone Call";
                    obj.Icons = "phone";
                }
                if (NoteId == 3)
                {
                    obj.NoteId = NoteId;
                    obj.NoteText = "Email";
                    obj.Icons = "envelope";
                }
                if (NoteId == 4)
                {
                    obj.NoteId = NoteId;
                    obj.NoteText = "Social Media";
                    obj.Icons = "users";
                }
                if (NoteId == 5)
                {
                    obj.NoteId = NoteId;
                    obj.NoteText = "Face To Face";
                    obj.Icons = "weixin";
                }
                obj.Description = NoteDes;
                DateTime time = DateTime.Now;
                obj.CreatedOn = time.ToString("T");
                obj.UserId = UserIdentity;
                db.Notes.Add(obj);
                db.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            var temp = db.Notes.Where(m => m.UserId == UserIdentity).ToList();
            return Json(temp, JsonRequestBehavior.AllowGet);         
        }

        public JsonResult DeleteSelNote(int? id, string date, int? UserIdentity)
        {
            List<Note> allnotes = new List<Note>();
            if (id != null)
            {
                var notedata = db.Notes.SingleOrDefault(s => s.NoteId == id && s.CreatedOn == date && s.UserId == UserIdentity);
                if (notedata != null)
                {
                    db.Notes.Remove(notedata);
                    db.SaveChanges();
                    date = "";
                    allnotes = db.Notes.Where(m => m.UserId == UserIdentity).ToList();
                    return Json(allnotes, JsonRequestBehavior.AllowGet);
                }
            }
            allnotes = db.Notes.Where(m => m.UserId == UserIdentity).ToList();
            return Json(allnotes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTask(ViewModelUserTextMessage AddUserTask,string LoanOffcierIds)
        {
            Task obj = new Task();
            var getAssigneeName = db.Applists.Where(m => m.Id == AddUserTask.Assignee).FirstOrDefault();
            obj.Assignee = getAssigneeName.AgentName;
            if (AddUserTask.Lead == 1)
            {              
                obj.Lead = "Phone Call";               
            }
            if (AddUserTask.Lead == 2)
            {
                obj.Lead = "E-Mail";
            }
            if (AddUserTask.Lead == 3)
            {
                obj.Lead = "Social Media";
            }
            if (AddUserTask.Lead == 4)
            {
                obj.Lead = "Face to Face";
            }
            if (AddUserTask.Lead == 5)
            {
                obj.Lead = "Text(SMS)";
            }
            if (AddUserTask.Lead == 6)
            {
                obj.Lead = "Administrative";
            }
            if (AddUserTask.Priority == 1)
            {
                obj.Priority = "Low";
            }
            if (AddUserTask.Priority == 2)
            {
                obj.Priority = "High";
            }
            if (AddUserTask.Priority == 3)
            {
                obj.Priority = "Critical";
            }
            obj.Description = AddUserTask.Description;
            obj.DueDate = AddUserTask.DueDate;
            obj.DueTime = AddUserTask.DueTime;       
            obj.IsActive = true;
            obj.IsCompleted = false;               
            obj.TaskTitle = AddUserTask.Title;
            obj.UserId = AddUserTask.UserID;
            var creDate = DateTime.Now;
            obj.CreatedDate = Convert.ToString(creDate.Date);
            var dd = Convert.ToDateTime(AddUserTask.DueDate);
            var thisYear = new DateTime(DateTime.Today.Year, dd.Month, dd.Day);
            var Day = thisYear.DayOfWeek;
            obj.TaskDay = Convert.ToString(Day);
            db.Tasks.Add(obj);
            db.SaveChanges();
            var UserData = db.users.Where(m => m.UserID == obj.UserId).FirstOrDefault();
            if(UserData != null)
            {
                UserData.IsTaskAssign = true;            
                db.Entry(UserData).State = EntityState.Modified;
                db.SaveChanges();
            }

            ModelState.Clear();    
            var AllTasks = db.Tasks.Where(m => m.UserId == obj.UserId && m.IsCompleted == false).ToList();
            int length = 0;
            if (AllTasks!=null)
            {            
                var DateCurrent = DateTime.Today;
                var date = Convert.ToString(DateCurrent.Date);
                var AllCount = db.Tasks.Where(m => m.UserId == obj.UserId && m.CreatedDate == date).Count();
                 length = AllCount;              
            }
            return Json(new { AllTask = AllTasks,TaskCount= length }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]        
        public ActionResult GetTaskDetails(int? UserId,int? Taskid)
        {
            var data = db.Tasks.Where(m => m.Id == Taskid && m.UserId == UserId).FirstOrDefault();           
            return Json(data);
        }

        [HttpPost]
        public ActionResult CompleteTask(int? UserId, int? Taskid, string LeadName)
        {
            var updatetask = db.Tasks.Where(m => m.Id == Taskid && m.UserId == UserId).FirstOrDefault();
            updatetask.IsCompleted = true;
            if (LeadName != null)
            {
                updatetask.Lead = LeadName;
            }
            else
            {
                updatetask.Lead = updatetask.Lead;
            }
            db.Entry(updatetask).State = EntityState.Modified;
           // db.Tasks.Add(updastask);
            db.SaveChanges();
            var DateCurrent = DateTime.Today;
            var date = Convert.ToString(DateCurrent.Date);
            var AllTaskRemaining = db.Tasks.Where(m=>m.UserId == UserId && m.IsCompleted == false).ToList();
            var CheckTaskExists = db.Tasks.Any(m => m.UserId == UserId && m.IsCompleted == true || m.UserId == UserId && m.IsActive == false);
            if (CheckTaskExists == false)
            {
                var getUserdetail = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
                getUserdetail.IsTaskAssign = false;

                db.Entry(getUserdetail).State = EntityState.Modified;
                db.SaveChanges();
            }
            var AllCount = db.Tasks.Where(m => m.UserId == UserId && m.CreatedDate == date && m.IsActive == true).Count();          
            return Json(new { AllTask = AllTaskRemaining, TaskCount = AllCount });
        }

        [HttpPost]
        public ActionResult deleteTask(int? UserId, int? Taskid)
        {       
                var deletetask = db.Tasks.Where(m => m.Id == Taskid && m.UserId == UserId).FirstOrDefault();
                deletetask.IsActive = false;
                db.Entry(deletetask).State = EntityState.Modified;
                db.SaveChanges();
                var DatesCurrent = DateTime.Today;
                var dates = Convert.ToString(DatesCurrent.Date);
                var AllTaskRemainings = db.Tasks.Where(m => m.UserId == UserId && m.IsCompleted == false).ToList();
                var AllCounts = db.Tasks.Where(m => m.UserId == UserId && m.CreatedDate == dates && m.IsActive == true).Count();
            var CheckTaskExists = db.Tasks.Any(m => m.UserId == UserId && m.IsCompleted == true || m.UserId == UserId && m.IsActive == false);
            if (CheckTaskExists == false)
            {
                var getUserdetail = db.users.Where(m => m.UserID == UserId).FirstOrDefault();
                getUserdetail.IsTaskAssign = false;
              
                db.Entry(getUserdetail).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { AllTasks = AllTaskRemainings, TaskCounts = AllCounts });           
        }

            #endregion User       
        }
}