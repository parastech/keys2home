//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Key2Home.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Applist
    {
        public int Id { get; set; }
        public string AppName { get; set; }
        public string ThemeColorCode { get; set; }
        public string AppLogo { get; set; }
        public string AgentName { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public string AppShortDiscription { get; set; }
        public string AppFullDiscription { get; set; }
    }
}
