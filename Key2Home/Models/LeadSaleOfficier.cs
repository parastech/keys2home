//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Key2Home.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LeadSaleOfficier
    {
        public int LeadSaleOfficierId { get; set; }
        public Nullable<int> LeadId { get; set; }
        public Nullable<int> SaleOfficierId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> Division { get; set; }
    }
}
