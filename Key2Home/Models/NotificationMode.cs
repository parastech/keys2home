//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Key2Home.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class NotificationMode
    {
        public int NotificationModeID { get; set; }
        public Nullable<bool> NotificationMode1 { get; set; }
        public Nullable<int> UserXID { get; set; }
        public string NotificationType { get; set; }
        public Nullable<System.DateTime> ChangedOn { get; set; }
    }
}
