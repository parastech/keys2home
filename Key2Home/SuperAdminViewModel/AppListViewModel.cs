﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Key2Home.SuperAdminViewModel
{


   public class AppListViewModel
    {
        public int AdminID { get; set; }
        public string AdminName { get; set; }
        public string AdminEmail { get; set; }
        public string AdminPassword { get; set; }
        public string AdminStatusID { get; set; }
        public string AdminImage { get; set; }
        public Nullable<System.DateTime> CreateOn { get; set; }
        public string PostDesc { get; set; }
        public string AdminAddress { get; set; }
        public string Type { get; set; }
        public string Phone { get; set; }
        public string MLSID { get; set; }
        public int Id { get; set; }
        public string AppName { get; set; }
        public string ThemeColorCode { get; set; }
        public string AppLogo { get; set; }
        public string AgentName { get; set; }
        public string AppShortDiscription { get; set; }
        public string AppFullDiscription { get; set; }
        public decimal? Plan { get; internal set; }
    }
}
