﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Key2Home.SuperAdminViewModel
{
    public class ChangePasswordViewModelSuperAdmin
    {
       
        public string OldPassword { get; set; }

        [RegularExpression(@"^((?!.*[\s])(?=.*[A-Z])(?=.*\d).{8,15})", ErrorMessage = "Password is Incorrect")]
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }


    }
}