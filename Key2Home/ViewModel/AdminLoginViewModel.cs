﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Key2Home.ViewModel
{
    public class AdminLoginViewModel
    {
        [Display(Name = "EmailId")]
        [Required(ErrorMessage = "Please Enter Your Emailid")]
        [RegularExpression("^([0-9a-zA-Z]*[üäöß]*([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", ErrorMessage = "E-mail is not valid")]
        [MaxLength(40)]
        public string EmailId { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please Enter Your Password")]
        [RegularExpression(@"^((?!.*[\s])(?=.*[A-Z])(?=.*\d).{8,15})", ErrorMessage = "Password is Incorrect")]
        [MaxLength(15)]
        public string Password { get; set; }

        public string Type { get; set; }
        public string SuperAdmin { get; set; }

       public bool RememberMe { get; set; }

    }

}