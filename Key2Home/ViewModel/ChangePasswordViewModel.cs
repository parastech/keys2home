﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Key2Home.ViewModel
{
    public class ChangePasswordViewModel
    {

        public string UserEmailId { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please Enter Your Old Password")]
        // [RegularExpression("^([0-9a-zA-Z]*[üäöß]*([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Please Enter Correct Old Password")]
        [RegularExpression(@"^((?!.*[\s])(?=.*[A-Z])(?=.*\d).{8,15})", ErrorMessage = "Please Enter Correct Old Password")]
        [Display(Name = "Old Password")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please Enter Your New Password")]
        [RegularExpression(@"^((?!.*[\s])(?=.*[A-Z])(?=.*\d).{8,15})", ErrorMessage = "Password is not strong")]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please Confirm Your New Password")]
        [Display(Name = "Confirm Password")]
        // [RegularExpression(@"^((?!.*[\s])(?=.*[A-Z])(?=.*\d).{8,15})",ErrorMessage ="")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The New Password and Confirm Password do not match")]
        public string ConfirmNewPassword { get; set; }
    }
}