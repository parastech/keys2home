﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Key2Home.Models;

namespace Key2Home.ViewModel
{
    public class ViewModelLeadDistribution
    {
        public int LeadId { get; set; }
        public string Title { get; set; }
        public string LeadType { get; set; }
        public string ActiveDays { get; set; }
        public Nullable<decimal> PriceMin { get; set; }
        public Nullable<decimal> PriceMax { get; set; }
        public string Locations { get; set; }
        public string LoanOfficerAssignId { get; set; }
        public string IsaAssignmentId { get; set; }
        public string SelLoanOfficier { get; set; }
        public string SelIsaAssignments { get; set; }
        public int AdminCount { get; set; }
        public int AdminIsaCount { get; set; }
        public bool IsActive { get; set; }
        public List<admin> _listAllAdmin = new List<admin>();

        public string LoanOffcierIds { get; set; }
        public string LoanOffcierQuanity { get; set; }
        public string LoanOffcierDivision { get; set; }

        public string SaleOffcierIds { get; set; }
        public string SaleOffcierQuanity { get; set; }
        public string SaleOffcierDivision { get; set; }
    }
}