﻿using Key2Home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Key2Home.ViewModel
{
    public class ViewModelSendMassMail
    {
            public string To { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public HttpPostedFileBase Attachment { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string  UserCount { get; set; }
        public string selUser { get; set; }

        public List<Template> _listAllTemplates = new List<Template>();
        public List<user> _listAllUsersEmails = new List<user>();

    }
}