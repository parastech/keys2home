﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Key2Home.ViewModel
{
    public class ViewModelUserMail
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Bcc { get; set; }
        public string Message { get; set; }
        public HttpPostedFileBase Attachment { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserCount { get; set; }
        public string selUser { get; set; }
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public Nullable<int> TeamXID { get; set; }
        public string Notes { get; set; }
        public string Warnings { get; set; }
        public bool SendWelcomeEmail { get; set; }
        public Nullable<System.DateTime> LastLogin { get; set; }
        public string SavSearch { get; set; }
        public string roles { get; set; }
        public string status { get; set; }
        public string pic { get; set; }
        public Nullable<int> AdminXID { get; set; }
        public string Location { get; set; }
        public string Mode { get; set; }
        public string ID { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public string SendFrom { get; set; }
        public string SendTo { get; set; }
        public string CreatedDate { get; set; }
    }
}
