﻿using Key2Home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Key2Home.ViewModel
{
    public class ViewModelUserTextMessage
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string SentDate { get; set; }
        public string UserName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public Nullable<int> TeamXID { get; set; }
        public string Notes { get; set; }
        public string Warnings { get; set; }
        public bool SendWelcomeEmail { get; set; }
        public Nullable<System.DateTime> LastLogin { get; set; }
        public string SavSearch { get; set; }
        public string roles { get; set; }
        public string status { get; set; }
        public string pic { get; set; }
        public Nullable<int> AdminXID { get; set; }
        public string Location { get; set; }
        public string Mode { get; set; }
        public int count { get; set; }

        public Nullable<bool> IsActive { get; set; }

        public List<TextMessage> _objMessageList = new List<TextMessage>();
    
        public List<Note> _objNoteList = new List<Note>();
        public int CampaignId { get; set; }
        public int AgentId { get; set; }
      
        public string CreatedDate { get; set; }
        public string EndDate { get; set; }
        public string Resolution { get; set; }
        public List<user> _objUser = new List<user>();

        public  List<Note> _listNote = new List<Note>();
        public  List<Note> _listSocial = new List<Note>();
        public  List<Note> _listFace = new List<Note>();
        public  List<Note> _listPhone = new List<Note>();
        public  List<Note> _listEmail = new List<Note>();

        public string Title { get; set; }
        public string Description { get; set; }
       // public string DueDate { get; set; }
        public string DueDate { get; set; }

        public string DueTime { get; set; }
        public int Assignee { get; set; }
        public int Priority { get; set; }
        public int Lead { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsActiveTask { get; set; }

        public List<Task> _listTasks = new List<Task>();


    }
}